C++ = g++
CFLAGS = -c -g

all: MLHdriver serviceCenterDriver

MLHdriver: 	MLHdriver.o ML_hash.o random_op.o 
		$(C++) -o MLHdriver MLHdriver.o ML_hash.o random_op.o

serviceCenterDriver: serviceCenterDriver.o serviceCenter.o Vehicle.o Car.o Hybrid.o Motorcycle.o Bus.o Tasks.o Prices.o ML_hash.o
		$(C++) -o serviceCenterDriver serviceCenterDriver.o serviceCenter.o Vehicle.o Car.o Hybrid.o Motorcycle.o Bus.o Tasks.o Prices.o ML_hash.o

clean:
		rm -f *.o 

%.o:	%.cpp
		$(C++) $(CFLAGS) $*.cpp

