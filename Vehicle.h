//Vehicle Class 

#ifndef VEHICLE_H
#define VEHICLE_H

#include <iostream> 

#include "Tasks.h"
#include "Prices.h"

using std::string; 
using std::ostream; 

class Tasks;

class Vehicle
{
	friend ostream &operator<<( ostream &, const Vehicle & ); 
public:
	Vehicle(); 
	Vehicle( int, int, string, int );
	virtual ~Vehicle(); 	
	void setID( int ); 
	void setModelYear( int ); 
	void setColor( string ); 	
	void setMileage( int ); 
	int getID() const;
	int getModelYear() const; 
	string getColor() const; 
	int getMileage() const;
	void selectTask(Prices); 
	virtual void printVehicle() const;
	void printTasks() const; 
	float checkOut();  
private:	
	int ID; 
	int ModelYear; 
	string Color; 
	int Mileage;
	Tasks *t; 
};
#endif
