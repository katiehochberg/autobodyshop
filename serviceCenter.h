//Service Center 

#ifndef SERVICECENTER_H
#define SERVICECENTER_H

#include <iostream>

#include "Vehicle.h"
#include "Car.h"
#include "Hybrid.h"
#include "Motorcycle.h"
#include "Bus.h" 
#include "Prices.h"
#include "Tasks.h"

using std::string; 
using std::cout; 
using std::cin; 
using std::endl; 

class serviceCenter
{
public:
	serviceCenter(); 
	~serviceCenter(); 
	void addVehicle(); 
	void report(); 
	void getVehicle();	 
private:
	MLH< Vehicle > *mlh;
	Prices p;
	float revenue;  
};
#endif 
