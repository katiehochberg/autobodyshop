#include <iostream>
#include "Bus.h"
#include "Vehicle.h"
using std::string; 

Bus::Bus( int id, int year, string color, int milage, int capacity ) : Vehicle( id, year, color, milage) 
{
	setPassenger(capacity); 
}

Bus::~Bus()
{
}

void Bus::setPassenger( int c ) 
{
	PassengerCapacity = c; 
}

int Bus::getPassenger() const 
{
	return PassengerCapacity; 
}

void Bus::printVehicle() const
{
	Vehicle::printVehicle(); 
	cout << "Passenger Capacity: " << PassengerCapacity << endl; 
}
