//Hybrid

#include "Hybrid.h"
#include "Car.h"
#include <iostream>

using std::string; 

Hybrid::Hybrid( int id, int year, string color, int milage, int engineSize, string gas, int pollution, int power, int usage, int capacity ) : Car( id, year, color, milage, engineSize, gas, pollution )
{
	setMotorPower( power ); 
	setMotorUsage( usage ); 
	setBatteryCapacity( capacity ); 
}

Hybrid::~Hybrid()
{
}

void Hybrid::setMotorPower( int p )
{
	MotorPower = p; 
}

void Hybrid::setMotorUsage( int u ) 
{
	MotorUsage = u; 
}

void Hybrid::setBatteryCapacity( int c ) 
{
	BatteryCapacity = c; 	
}

int Hybrid::getMotorPower() const
{
	return MotorPower;
}

int Hybrid::getMotorUsage() const
{
	return MotorUsage;
}

int Hybrid::getBatteryCapacity() const
{
	return BatteryCapacity;
}

void Hybrid::printVehicle() const
{
	Car::printVehicle(); 
	cout << "Motor Power: " << MotorPower << endl; 
	cout << "Motor Usage: " << MotorUsage << endl; 
	cout << "Battery Capacity: " << BatteryCapacity << endl; 
}	
