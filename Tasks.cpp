//Tasks.cpp
#include "Tasks.h"
#include "Prices.h"

#include <iostream> 

using std::cout; 
using std::cin; 
using std::endl; 
using std::string; 

TasksList::TasksList( const data &data ) 
	: content( data ), next( NULL ) 
{
	//Empty Body
}

data TasksList::getTasksList() const 
{
	return content; 
}

//Tasks constructor 
//Sets the first of the linked list to NULL
Tasks::Tasks() 
{
	first = NULL;
}

//Tasks destructor
//Will delete each node of the linked list 
Tasks::~Tasks()
{
	TasksList *ptr; 
	while( first != NULL )
	{
		ptr = first; 
		first = ptr->next; 
		delete ptr; 
		cout << "Destructor: removing a node" << endl; 
	}
}
 
void Tasks::selectTask(Prices p)
{
	int choice;
	bool again = true;  
	do {
		cout << "Please select the services needed for this vehicle:" << endl; 
		cout << "---------------------------------------------------" << endl; 
		cout << "1. Scheduled Maintenance" << endl; 
		cout << "2. Brake Change" << endl; 
		cout << "3. Tune-up" << endl; 
		cout << "Please enter choice: "; 
		cin >> choice;
		if( choice < 1 || choice > 3 ) 
		{
			cout << "Invalid choice! Please choose again: ";
		}
		else 
		{
			data t; 
			t.name = p.getTasks(choice); 
			t.laborCost = p.getLabor(choice); 
			t.partsCost = p.getParts(choice); 
			addTask(t); 	
			cout << "Would you like to add another task?" << endl; 
			cout << "Enter 1 for yes or 2 for no: "; 
			cin >> choice; 
			if( choice == 2 ) again = false; 
		}
	} while( again == true ); 
}

void Tasks::addTask( const data &t ) 
{
	TasksList *ptr = new TasksList(t); 
	ptr->next = first; 
	first = ptr;  	
}

void Tasks::print() const
{
	TasksList *ptr;
	int i = 1;  
	data t; 
	cout << "Services rendered for this vehicle: ";
	ptr = first;
	if( ptr == NULL ) 
	{
		cout << "\nEmpty task list!" << endl;
	} 
	while( ptr != NULL )
	{
		t = ptr->getTasksList(); 
		cout << "\n"<< i << ". " <<t.name << "...Labor: $" << t.laborCost << "...Parts: $" << t.partsCost;
		ptr = ptr->next;
		i++;  
	}	
}

float Tasks::checkOut()
{
	TasksList *ptr; 
	ptr = first; 
	data t;
	float p = 0; 
	while( ptr != NULL ) 
	{
		t = ptr->getTasksList(); 
		p += t.laborCost; 
		p += t.partsCost;
		ptr = ptr->next;
	}
	return p; 
}
