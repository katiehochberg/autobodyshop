//Motorcycle

#include "Motorcycle.h"
#include <iostream>

using std::string; 

Motorcycle::Motorcycle( int id, int year, string color, int milage, int engineSize, int front, int back ) : Vehicle( id, year, color, milage ) 
{
	setEngineSize( engineSize ); 
	setFront( front ); 
	setBack( back ); 
};	

Motorcycle::~Motorcycle()
{
}

void Motorcycle::setEngineSize( int size )
{
	EngineSize = size;
}

void Motorcycle::setFront( int f )
{
	FrontWheel = f; 
}

void Motorcycle::setBack( int b )
{
	BackWheel = b; 
}

int Motorcycle::getEngineSize() const 
{
	return EngineSize; 
}

int Motorcycle::getFront() const
{
	return FrontWheel; 
}

int Motorcycle::getBack() const
{
	return BackWheel;
}

void Motorcycle::printVehicle() const 
{
	Vehicle::printVehicle(); 
	cout << "Engine Size: " << EngineSize << endl; 
	cout << "Front Wheel Size: " << FrontWheel << endl; 
	cout << "Back Wheel Size: " << BackWheel << endl; 
}
