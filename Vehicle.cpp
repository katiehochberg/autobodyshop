#include "Vehicle.h"
#include "Tasks.h"
#include <iostream>

using std::string;
using std::ostream; 
using std::cout; 
using std::endl;  

Vehicle::Vehicle()
{ 
}

Vehicle::Vehicle( int id, int year, string color, int mileage ) 
{
	setID(id); 
	setModelYear(year); 
	setColor(color); 
	setMileage(mileage);
	t = new Tasks();  
}

Vehicle::~Vehicle() 
{ 
	delete t; 
}

void Vehicle::setID( int id )
{
	ID = id; 
}

void Vehicle::setModelYear( int year ) 
{
	ModelYear = year; 
}

void Vehicle::setColor( string color )
{
	Color = color; 
}

void Vehicle::setMileage( int mileage )
{
	Mileage = mileage; 
}

int Vehicle::getID() const
{
	return ID;
}	

int Vehicle::getModelYear() const
{
	return ModelYear; 
}

string Vehicle::getColor() const
{
	return Color;
}

int Vehicle::getMileage() const
{
	return Mileage; 
}

void Vehicle::selectTask(Prices p) 
{
	t->selectTask(p); 	
}

float Vehicle::checkOut() 
{
	int i = t->checkOut(); 
	delete t;  
	return i; 
}

void Vehicle::printVehicle() const  
{
	cout << "\nVehicle:" << endl;  
	cout << "ID: " << ID << endl; 
	cout << "Model Year: " << ModelYear << endl; 
	cout << "Color: " << Color << endl; 
	cout << "Mileage: " << Mileage << endl;
}

void Vehicle::printTasks() const 
{
	t->print();  
}

ostream& operator<< ( ostream &output, const Vehicle &v1 )
{
	v1.printVehicle(); 
	v1.printTasks(); 	 
	output;
	return output; 
}
