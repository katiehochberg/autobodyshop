//Bus Class

#ifndef BUS_H
#define BUS_H

#include <iostream> 
#include "Vehicle.h"

using std::string; 

class Bus : public Vehicle
{
public:
	Bus( int, int, string, int, int ); 
	~Bus(); 
	void setPassenger( int );
	int getPassenger() const; 
	virtual void printVehicle() const;   
private:
	int PassengerCapacity;
};
#endif
