#include "Vehicle.h" 
#include "Car.h"
#include "Hybrid.h"
#include "Motorcycle.h"
#include "Bus.h"
#include "MLH.h"
#include "serviceCenter.h"

#include <iostream>

using std::cout; 
using std::cin; 
using std::endl; 
using std::string; 

int main() 
{
	serviceCenter sc;
	int choice; 	
	cout << "Welcome to the Service Center!" << endl; 
	cout << "------------------------------" << endl; 
	
	do {
		cout << "Please select a menu option: " << endl; 
		cout << "-----------------------------" << endl;
		cout << "1. New vehicle " << endl; 
		cout << "2. Report of vehicles currently in shop" << endl;
		cout << "3. View status of a specific vehicle" << endl; 
		cout << "4. Quit" << endl;
		cout << "Please enter choice: "; 
		cin >> choice;
		cout << "--------------------" << endl;   
		switch ( choice ) 
		{
			case 1: 
				sc.addVehicle(); 
				break;
			case 2:	
				sc.report();
				break; 
			case 3: 
				sc.getVehicle(); 	
				break;
			case 4: 
				break; 
			default:	
				cout << "Invalid choice!" << endl; 
		}
	} while( choice != 4 );  
	return 0; 
}
