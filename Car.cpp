#include "Car.h"
#include "Vehicle.h"
#include "Tasks.h"

#include <iostream>

using std::string; 
using std::cout; 
using std::endl;

Car::Car( int id, int year, string color, int milage, int engineSize, string gas, int pollution ) : Vehicle( id, year, color, milage )
{ 
	setEngineSize( engineSize ); 
	setGas( gas ); 
	setEnginePollution( pollution );
}

Car::~Car()
{ 
}

void Car::setEngineSize( int size )
{
	EngineSize = size;
}

void Car::setGas( string gas ) 
{
	Gas = gas; 
}

void Car::setEnginePollution( int pollution )
{
	EnginePollution = pollution;
}

int Car::getEngineSize() const
{
	return EngineSize; 
}

string Car::getGas() const
{
	return Gas; 
}

int Car::getEnginePollution() const
{
	return EnginePollution;
}

void Car::printVehicle() const
{
	Vehicle::printVehicle(); 
	cout << "Engine Size: " << EngineSize << endl; 
	cout << "Gas: " << Gas << endl; 
	cout << "Engine Pollution: " << EnginePollution << endl; 
}
