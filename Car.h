//Car class

#ifndef CAR_H
#define CAR_H

#include <iostream>
#include "Vehicle.h"
#include "Tasks.h"

using std::string;

class Car : public Vehicle
{
public:
	Car( int, int, string, int, int, string, int );
	 ~Car(); 
	void setEngineSize( int ); 
	void setGas( string ); 
	void setEnginePollution( int );
	int getEngineSize() const; 
	string getGas() const; 
	int getEnginePollution() const;
	virtual void printVehicle() const;  
private:
	int EngineSize; 
	string Gas; 
	int EnginePollution;
};
#endif
