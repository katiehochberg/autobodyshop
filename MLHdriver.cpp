//Driver Program for Multi-Level Hash Data Structure 

#include <iostream>
#include "MLH.h"
#include "random_op.h"

using std::cin; 
using std::cout; 
using std::endl; 

int main() 
{
	MLH< int > *mlh = new MLH< int >;	
	unsigned int seed; 
	int range;
	int numOp; 
	int printOption; 
	cout<< "Welcome to the Multi-Level Hash Program!" << endl; 
	cout<< "----------------------------------------" << endl; 
	cout<< "Please enter the number of operations requested: "; 
	cin>> numOp;
	cout<< "Please enter a range: "; 
	cin>> range; 
	cout<< "Please enter a starting seed: "; 
	cin>> seed;
	cout<< "Would you like to print the keys and data in the array? " << endl;
	cout<< "Enter 1 for yes or 2 for no: "; 
	cin>> printOption;
	if( printOption == 1 ) mlh->MLH_set_print_option(2);   
	cout<< "\nGenerating " << numOp << " operations with range " << range << " and random seed " << seed << endl;
	cout<< "--------------------------------------------------------------" << endl;
	
	Random_operation r( seed, range ); 
	
	int i = 0; 
	char op = 'a';  
	int key = 0; 
	int *data;
	for( i = 1; i <= numOp; i++ )
	{ 
		op = r.Get_op();
		key = r.Get_key(); 
		data = new int;  
		*data = i; 
		int m = 1; 
		switch( op ) 
		{
			case 'G': 
				mlh->MLH_get( key );
				delete data; 
				break; 
			case 'I':
				m = mlh->MLH_insert( key, data );
				if ( m == 0 ) delete data;  
				break;
			case 'D':	
				mlh->MLH_delete( key );  
				delete data; 
				break; 
		} 
		if( i % (numOp/10 ) == 0 )
		{
			cout<< "\nPrinting after " << i << " operations" << endl;
			cout<< "-----------------------------" << endl;
			cout << (*mlh); 
			cout<< endl; 
		} 
		r.Randomize_next_op();     
	} 
	delete mlh;
	return 0;
}
