//Prices.cpp

#include <iostream>
#include "Prices.h"

using std::cout; 
using std::cin; 
using std::endl; 
using std::string; 

//Constructor 
//Will fill in the prices and tasks array with prices and names of the tasks
//that can be performed in the service center

Prices::Prices() 
{
	tasks[0] = ""; 
	tasks[1] = "Scheduled Maintenance"; 
	tasks[2] = "Brake Change"; 
	tasks[3] = "Tune-up"; 
	
	parts[0] = 0; 
	parts[1] = 100; 
	parts[2] = 150; 
	parts[3] = 125; 
	
	labor[0] = 0; 
	labor[1] = 25; 
	labor[2] = 30;
	labor[3] = 50; 	
}

float Prices::getParts(int i) const
{
	return parts[i];
}

float Prices::getLabor(int i) const 
{
	return labor[i]; 
}

string Prices::getTasks(int i) const
{
	return tasks[i]; 
}


