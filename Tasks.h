//Service Center 

#ifndef TASKS_H
#define TASKS_H

#include <iostream>
#include "Prices.h"
#define numberTasks 3 //number of tasks there are to do 

using std::string; 
using std::cout; 
using std::cin; 
using std::endl; 

typedef struct dummy_node { 
	string name; 
	float laborCost; 
	float partsCost; 
} data; 

class TasksList 
{
friend class Tasks;
public:
	TasksList( const data &data ); 
	data getTasksList() const; 
private:
	data content; 
	TasksList *next; 
};

class Tasks
{
public:
	Tasks(); 
	~Tasks(); 
	void selectTask(Prices); 
	void addTask(const data&); 
	void print() const; 
	float checkOut();
private:
	string name;
	float laborCost; 
	float partsCost; 

	//Head of the List
	TasksList *first;  
};	

#endif 
