//Motorcycle Class

#ifndef MOTORCYCLE_H
#define MOTORCYCLE_H

#include "Vehicle.h"
#include <iostream>

using std::string;

class Motorcycle : public Vehicle 
{
public:
	Motorcycle( int, int, string, int, int, int, int ); 
	~Motorcycle(); 
	void setEngineSize( int ); 
	void setFront( int ); 
	void setBack( int );
	int getEngineSize() const;
	int getFront() const; 
	int getBack() const;
	virtual void printVehicle() const;  
private:
	int EngineSize; 
	int FrontWheel;
	int BackWheel;
};
#endif
