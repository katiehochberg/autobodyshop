#include "Vehicle.h" 
#include "Car.h"
#include "Hybrid.h"
#include "Motorcycle.h"
#include "Bus.h"
#include "MLH.h"
#include "serviceCenter.h"

#include <iostream>

using std::cout; 
using std::cin; 
using std::endl; 
using std::string; 

serviceCenter::serviceCenter() 
{
	mlh = new MLH< Vehicle >;
	mlh->MLH_set_print_option(2);  
	Prices p;	
	revenue = 0; 
}

serviceCenter::~serviceCenter()
{ 
	delete mlh; 
}

void serviceCenter::addVehicle() 
{
	int id; 
	int year; 
	string color; 
	int mileage;
	int choice; 
	Vehicle *v;  
	do {
		cout << "Please enter new vehicle ID: "; 
		cin >> id;
		v = NULL; 
		v = mlh->MLH_get(id); 
		if( v != NULL ) 
			cout << "ID has already been used!" << endl;
	} while( v != NULL ); 
	v == NULL;
	do {
		cout << "Please enter model year (range 2000...2014): "; 
		cin >> year;
	} while( year < 2000 || year > 2014 );  
	cout << "Please enter color: "; 
	cin >> color; 
	cout << "Please enter mileage: ";
	cin >> mileage; 

	int eSize; 
	string gas; 
	int pollution; 
	int motorPower; 
	int usage; 
	int capacity; 
	int front; 
	int back; 
	int passengers; 
	//We should ask them if they want to make a certain type
	bool again = false; 
	int k;
	do {
		again = false; 	
		cout << "Is this vehcile a: " << endl; 	
		cout << "------------------ " << endl;
		cout << "1. Car " << endl; 
		cout << "2. Motorcycle" << endl; 
		cout << "3. Bus" << endl; 
		cout << "4. None of the above" <<endl;
		cout << "Please enter choice: "; 
		cin >> choice;
		cout << "--------------------" << endl; 
		switch (choice) 
		{
			case 1:
				cout << "Please enter engine size (cc): "; 
				cin >> eSize;
				cout << "Please enter gas type (premium, plus, regular): "; 
				cin >> gas;	
				cout << "Please enter engine pollution level: "; 
				cin >> pollution;
				do {
					cout << "Is this car a hybrid? Enter 1 for yes or 2 for no: "; 
					cin >> choice; 
					if( choice != 1 && choice != 2 )
						cout << "Invalid choice!" << endl;
				} while( choice != 1 && choice != 2 ); 
				if( choice == 1 ) 
				{ 
					cout << "Please enter motor power: "; 
					cin >> motorPower; 
					cout << "Please enter overall usage (in hours): "; 
					cin >> usage;
					cout << "Please enter battery capacity: "; 
					cin >> capacity;
					v = new Hybrid( id, year, color, mileage, eSize, gas, pollution, motorPower, usage, capacity); 
				}
				else 
				{
					v = new Car(id, year, color, mileage, eSize, gas, pollution);
				}
				break;
			case 2:
				cout << "Please enter engine size (cc): "; 
				cin >> eSize; 
				cout << "Please enter front wheel size: "; 
				cin >> front; 
				cout << "Please enter back wheel size: ";
				cin >> back; 
				v = new Motorcycle( id, year, color, mileage, eSize, front, back ); 
				break;
			case 3: 
				cout << "Please enter passenger capacity: ";
				cin >> passengers; 
				v = new Bus( id, year, color, mileage, passengers );
				break; 
			case 4:			
				v = new Vehicle( id, year, color, mileage );
				break; 
			default:
				cout << "Invalid choice!" << endl; 
				again = true; 
				break;
		}
	} while( again == true ); 
	v->selectTask(p);
	k = mlh->MLH_insert( id, v );  	
	if( k == 0 ) delete v; 
}

void serviceCenter::report() 
{
	cout << (*mlh);	
	cout << "\n========================" << endl;
	cout << "Total Revenue: $" << revenue << endl; 
	cout << "========================\n" << endl; 
}	

void serviceCenter::getVehicle()
{
	Vehicle *v;
	int c;
	cout << "Please enter the ID of the specific vehicle: "; 
	cin >> c; 
	v = mlh->MLH_get(c);
	if( v != NULL ) 
	{ 
		v->printVehicle();
		v->printTasks();  
	}
	else
	{
		cout << "Vehicle with ID " << c << " was not found in the Service Center!" << endl;
	}
	do {
		cout << "Would you like to: " << endl; 
		cout << "------------------ " << endl;
		cout << "1. Add tasks to the vehicle" << endl; 
		cout << "2. Checkout vehicle" << endl; 
		cout << "3. Quit " << endl; 
		cout << "Please enter choice: "; 
		cin >> c; 
		switch (c) 
		{
			case 1:
				v->selectTask(p);
				cout << "Tasks performed on this vehicle: " << endl; 
				v->printTasks(); 
				break;
			case 2:
				revenue += v->checkOut(); 	
				cout << "Vehicle " << v->getID() << " checked out!" << endl; 
				cout << "Total Revenue: $" << revenue << endl;			
				mlh->MLH_delete( v->getID() ); 	
				break; 
			case 3:
				break; 
			default:
				cout << "Invalid choice!" << endl; 
				break;
		}
	} while( c < 1 || c > 3 );
}

