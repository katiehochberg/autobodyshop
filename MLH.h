//Template file for Multi-Level Hash Data Structure

#ifndef MLH_H
#define MLH_H

#include <iostream>
#include "ML_hash.h"

using std::cout; 
using std::endl;
using std::ostream;

#define nodeSize 5

template< typename T > class MLH;

template< typename T > 
class MLHnode 
{

friend class MLH< T >;

public:
	MLHnode(); 
private:
	int key[nodeSize + 1]; 
	T *data[nodeSize + 1]; 
	MLHnode *child[nodeSize + 1];
	MLHnode *previous; 
	int size; 
}; 

#define nodeSize 5

template< typename T >
ostream& operator<<(ostream &out, const MLH< T > &t); 

template< typename T >
class MLH
{

friend ostream& operator<<<>(ostream &o, const MLH< T > &t); 

public:
	MLH(); 
	~MLH(); 
	int MLH_insert( const int, T* ); 
	int MLH_delete( int ); 
	T* MLH_get( int );
	int  MLH_getSize(MLHnode< T >*) const; 
	void MLH_print(MLHnode< T >*) const; 
	void MLH_set_print_option( int );		
	void MLH_freeNode(MLHnode< T >*);  
private:
	void MLH_explode(MLHnode< T >*, int);
	void MLH_collapse(MLHnode< T >*, int);  
	MLHnode< T > *head; 
	int counter; 
	int numberNodes; 
	int printOption;
};

#endif

template< typename T >
MLHnode< T >::MLHnode()
{
	int i = 0; 
	for( i = 0; i < nodeSize + 1; i++ )
	{
		key[i] = 0; 
		data[i] = NULL; 
		child[i] = NULL;
	} 
	previous = NULL;
	size = 0;	
}

template< typename T > 
MLH< T >::MLH()
{
	head = new MLHnode< T >;   
	counter = 0; 
	MLH_set_print_option(1); 
	numberNodes = 1; 
} 

template< typename T > 
MLH< T >::~MLH() 
{
	MLH_freeNode(head); 	
}

template< typename T > 
int MLH< T >::MLH_getSize(MLHnode< T > *t) const
{
	return t->size; 
}

template< typename T > 
int MLH< T >::MLH_insert( const int key, T *data ) 
{
	MLHnode< T > *current;
	MLHnode< T > *ptr;  
	current = head; 
	int currentLevel = 1; 
	int i = 0; //used for for loops
	int n = 0; //used for ML_hash returns
	
	while( current != NULL ) 
	{
		if( current->size >= nodeSize ) 	
		{
			if( current->size == nodeSize ) 	
				{	
				for( i = 1; i < nodeSize + 1; i++ ) 
				{
					if( current->key[i] == key ) return 0; 
				}
				MLH_explode(current, currentLevel);	
			}
			n = ML_hash(currentLevel, key);
			if( current->child[n] == NULL )
				ptr = current; 
			current = current->child[n]; 
			counter++;
			currentLevel++; 
		}
		else break;
	}
	if( current == NULL ) //we need to make a new node
	{
		current = new MLHnode< T >; 
		current->key[1] = key; 
		current->data[1] = data; 
		current->size++;
		ptr->child[n] = current; 
		current->previous = ptr;
		numberNodes++; 
	}
	else if( current->size < nodeSize ) 
	{	
		i = 1;
		while( current->key[i] != 0 ) 
		{
			if( current->key[i] == key ) return 0; 
			i++;
		}
		current->key[i] = key; 
		current->data[i] = data;
		current->size++;  
	}
	while( current->previous != NULL )
	{
		current = current->previous; 
		current->size++; 
	}
	return 1;  
}

template< typename T > 
int MLH< T >::MLH_delete( int key )
{
	int i;
	int n; 
	int j;
	MLHnode< T > *ptr; 
	MLHnode< T > *current; 
	current = head; 
	int currentLevel = 1;
	T *d; 
	while( current != NULL )
	{
		if( current->size <= nodeSize ) 
		{
			i = 1; 
			while( current->key[i] != 0 ) 
			{
				if( current->key[i] == key && i <= nodeSize ) 
				{
					d = current->data[i];
					current->key[i] = 0; 
					current->data[i] = NULL;
					current->size--; 
					//Shifting array 
					if( i != 5 && current->key[i+1] != 0 )
					{
						
						for( i; i < nodeSize; i++ ) 
						{	
							current->key[i] = current->key[i+1]; 
							current->data[i] = current->data[i+1]; 
						}
						current->key[nodeSize] = 0; 
						current->data[nodeSize] = NULL;
					} 
					while( current->previous != NULL ) 
					{
						ptr = current; 
						current = current->previous;
						currentLevel--;
						current->size--;
						if( ptr->size == 0 )
						{
							current->child[n] = NULL; 
							numberNodes--;
							delete ptr; 
						}
						if( current->size == nodeSize)
							MLH_collapse(current, currentLevel);
					}
					delete d; 
					return 1;
				}
				i++; 
			}
			return 0; 
			
		}
		else
		{	
			n = ML_hash( currentLevel, key ); 
			current = current->child[n];
			counter++;
			currentLevel++;  
		}
	}
	return 0;
} 

template< typename T > 
T* MLH< T >::MLH_get( int key ) 
{
	if( head->size == 0 ) return NULL; 
	int i;
	int n;
	int currentLevel = 1; 
	MLHnode< T > *current;
	current = head; 
	while( current != NULL ) 
	{
		if( current->size <= nodeSize )
		{
			i = 1; 
			while( current->key[i] != 0 ) 
			{
				if( current->key[i] == key ) 
				{
					return current->data[i]; 
				}
				i++; 
			}	
			current = NULL; 
		}	
		else if( current->size > nodeSize )
		{
			n = ML_hash( currentLevel, key ); 
			current = current->child[n];	
			counter++;
			currentLevel++; 	
		}
	}	
	return NULL;
}

template< typename T > 
void MLH< T >::MLH_explode(MLHnode< T > *current, int currentLevel) 
{
	int i;
	int j; 
	int n;   
	for( i = 1; i < nodeSize + 1; i++ ) 
	{
		n = ML_hash(currentLevel, current->key[i]);
		if( current->child[n] == NULL ) 
		{
			MLHnode< T > *newNode = new MLHnode< T >; 
			newNode->key[1] = current->key[i];
			newNode->data[1] = current->data[i];
			newNode->size++;
			current->child[n] = newNode; 
			newNode->previous = current;				 
			numberNodes++;	
		} 
		else
		{  
			j = current->child[n]->size; 
			current->child[n]->key[ j + 1 ] = current->key[i];
			current->child[n]->data[ j + 1 ] = current->data[i];
			current->child[n]->size++;
		}
	}
}

template< typename T > 
void MLH< T >::MLH_collapse(MLHnode< T > *current, int currentLevel)
{
	int i;
	int j;
	MLHnode< T > *ptr;  
	int c = 1; //spot of current node where there is space
	for( i = 1; i < nodeSize + 1; i++ ) 
	{
		ptr = current->child[i];
		if( ptr != NULL )
		{
			j = 1; 
			while( ptr->key[j] != 0 )
			{
				current->key[c] = ptr->key[j]; 
				current->data[c] = ptr->data[j];
				c++;
				j++;  
			}
			//need to delete the node 
			delete current->child[i]; 
			current->child[i] = NULL;
			numberNodes--;
		}
	} 	 
}

template< typename T > 
void MLH< T >::MLH_print(MLHnode< T > *ptr) const
{ 
	int i;  
	if( ptr != NULL ) 
	{ 
		if( ptr->size > nodeSize )
			for( i = 1; i < nodeSize + 1; i++ ) 
			{
				MLH_print(ptr->child[i]); 
			}
		else
		{
			i = 1; 
			while( ptr->key[i] != 0 && i <= nodeSize ) 
			{ 
				cout << "Key: " << ptr->key[i] << " Data: " << *ptr->data[i] << endl;
				cout << "-----------" << endl;  
				i++; 
			}
		}  
	} 
}

template< typename T > 
void MLH< T >::MLH_set_print_option( int p )
{
	printOption = p; 
}

template< typename T >
void MLH< T >::MLH_freeNode(MLHnode< T > *current) 
{
	MLHnode< T > *ptr; 
	ptr = current; 
	int i; 
	int j; 
	if( ptr->size > nodeSize ) 
	{
		for( j = 1; j < nodeSize + 1; j++ )
		{
			if( ptr->child[j] != NULL ) 
				MLH_freeNode(ptr->child[j]); 
		}	
	}
	else 
	{
		for( i = 1; i < nodeSize + 1; i++ )
		{
			delete ptr->data[i];
			ptr->data[i] = NULL;
		}
	}
	delete ptr;
	ptr = NULL;
}

template< typename T > 
ostream& operator<< (ostream &out, const MLH< T > &t) 
{
	if( t.printOption == 2 )  
		t.MLH_print(t.head);  
	out << "Number of objects: " << t.MLH_getSize(t.head) << endl << "Number of nodes: " << t.numberNodes << endl << "Number of next steps: " << t.counter << endl;  
	return out;

}
