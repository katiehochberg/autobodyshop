//Hybrid Class

#ifndef HYBRID_H
#define HYBRID_H

#include <iostream>
#include "Car.h"
//#include "Vehicle.h"

class Hybrid : public Car 
{
public:
	Hybrid( int, int, string, int, int, string, int, int, int, int ); 
	~Hybrid(); 
	void setMotorPower( int );
	void setMotorUsage( int ); 
	void setBatteryCapacity( int );
	int getMotorPower() const; 
	int getMotorUsage() const; 
	int getBatteryCapacity() const;
	virtual void printVehicle() const; 
private:
	int MotorPower;
	int MotorUsage;
	int BatteryCapacity; 
};
#endif 
