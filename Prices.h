//Prices class:
//Will keep track of the services offered at the service center and their prices
//for labor and parts

#ifndef PRICES_H
#define PRICES_H

#include <iostream>

using std::string; 
using std::cout; 
using std::cin; 
using std::endl; 

#define s 3 //number of tasks able to be done 

class Prices
{
public:
	Prices();  
	float getParts( int ) const; 
	float getLabor( int ) const; 
	string getTasks( int ) const;  
private:
	float parts[s + 1];
	float labor[s + 1]; 
	string tasks[s + 1];
	
};

#endif 
